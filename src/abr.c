#include <stdio.h>
#include <stdlib.h>

#include "../lib/abr.h"
#include "../lib/pile.h"
#include "../lib/file.h"


#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
#define abs(a) ((a)<0?(-a):(a))


int feuille (Arbre_t a) {
  if (a == NULL)
    return 0 ;
  else
    {
      if ((a->fgauche == NULL) && (a->fdroite == NULL))
	return 1 ;
      else
	return 0 ;
    }
}

Arbre_t ajouter_noeud (Arbre_t a, Arbre_t n) {
  /* ajouter le noeud n dans l'arbre a */
  
  if (a == NULL)
    return n ;
  else if (n->cle < a->cle)
	a->fgauche = ajouter_noeud (a->fgauche, n);
  else
	a->fdroite = ajouter_noeud (a->fdroite, n);
  return a ;
  
}  

Arbre_t rechercher_cle_arbre (Arbre_t a, int valeur) {
  if (a == NULL)
    return NULL ;
  else
    {
      if (a->cle == valeur)
	return a ;
      else
	{
	  if (a->cle < valeur)
	    return rechercher_cle_arbre (a->fdroite, valeur);
	  else
	    return rechercher_cle_arbre (a->fgauche, valeur);
	}
    }
}

Arbre_t ajouter_cle (Arbre_t a, int cle) {
  Arbre_t n ;
  Arbre_t ptrouve ;
  
  /* 
     ajout de la clé. Creation du noeud n qu'on insere 
    dans l'arbre a
  */

  ptrouve = rechercher_cle_arbre (a, cle);

  if (ptrouve == NULL)
    {
      n = (Arbre_t) malloc (sizeof(noeud_t));
      n->cle = cle;
      n->fgauche = NULL ;
      n->fdroite = NULL ;

      a = ajouter_noeud (a, n);
      return a ;
    }
  else
    return a ;
}


Arbre_t lire_arbre (char *nom_fichier) {
  FILE *f ;
  int cle;
  Arbre_t a = NULL;
  
  f = fopen (nom_fichier, "r");

  while (fscanf (f, "%d", &cle) != EOF)
    {
      a = ajouter_cle (a, cle);
    }
    
  fclose (f);

  return a ;
}

void afficher_arbre (Arbre_t a, int niveau) {
  /*
    affichage de l'arbre a
    on l'affiche en le penchant sur sa gauche
    la partie droite (haute) se retrouve en l'air
  */
  
  int i ;
  
  if (a != NULL)
      {
	afficher_arbre (a->fdroite,niveau+1);
	
	for (i = 0; i < niveau; i++)
	  printf ("\t");
	printf (" %d (%d)\n\n", a->cle, niveau);

	afficher_arbre (a->fgauche, niveau+1);
      }
  return ;
}

/**********************************************************************************************/

int hauteur_arbre_r (Arbre_t a) {
  /*if (feuille(a)) SEE
    return 0;
  int c=-1;
  int b=-1;
  if (a->fgauche!=NULL)
    c =hauteur_arbre_r(a->fgauche);
  if (a->fdroite!=NULL)
    b =hauteur_arbre_r(a->fdroite);
  return 1+max(c,b);*/

  int hauteur = -1;
  if(a != NULL) {      // pas de noeud
    if(feuille(a))    // noeud externe
      hauteur = 0;
    else {            // noeud interne
      int hauteurG = hauteur_arbre_r(a->fgauche);
      int hauteurD = hauteur_arbre_r(a->fdroite);
      hauteur = max(hauteurG, hauteurD)+1;
    }
  }
  return hauteur;
}

int hauteur_arbre_nr (Arbre_t a) {
  /*  On utilise plusieurs compteurs pour
   *  déterminer la hauteur max de l'arbre.
   */
  pfile_t f = creer_file();
  int n = 0;

  int max = -1;
  int niveau = 1;
  enfiler(f,a);
  while (!file_vide(f)) {
    Arbre_t a = defiler(f);
    niveau--;
    if (a->fgauche != NULL) {
      enfiler(f,a->fgauche);
      n++;
    }
    if (a->fdroite != NULL) {
      enfiler(f, a->fdroite);
      n++;
    }
    if(niveau == 0) {
      niveau = n;
      n = 0;
      max++;
    }
  }
  return max ;
}


void parcourir_arbre_largeur (Arbre_t a) {
  /*  On utilise une file pour parcourir
   *  l'arbre en largeur.
   */
  printf("parcours :\n");
  pfile_t f = creer_file();
  enfiler(f, a);
  while (!file_vide(f)) {
    Arbre_t sorti = defiler(f);
    printf("Element: %d \n", sorti->cle);

    if (sorti->fgauche != NULL)
      enfiler(f, sorti->fgauche);
    if (sorti->fdroite != NULL)
      enfiler(f, sorti->fdroite);
  }
  detruire_file(f);
  return ;
}

void afficher_nombre_noeuds_par_niveau (Arbre_t a) {
  /*  Affiche le nombre de noeuds par niveau après
   *  avec totalement parcouru chaque niveau grâce
   *  à la condition ligne 229 SEE (mettre la ligne correcte après éventuelle modification)
   */
  pfile_t f = creer_file();
  int n = 0;
  int niveau = 1;
  printf("%d ", niveau);
  enfiler(f, a);
  while (!file_vide(f)) {
    Arbre_t a = defiler(f);
    niveau--;
    if (a->fgauche != NULL) {
      enfiler(f, a->fgauche);
      n++;
    }
    if (a->fdroite != NULL) {
      enfiler(f, a->fdroite);
      n++;
    }
    if((niveau == 0) && (!file_vide(f))) {
      niveau = n;
      printf("niveau: %d ", niveau);
      n = 0;
    }
  }
}


int nombre_cles_arbre_r (Arbre_t a) {/*   SEE
  if (feuille(a)) {  //si a est une feuille, 
    return 1;       //il n'y a qu'une clé.
  } 
  int c=0;
  int b=0;
  if (a->fgauche!=NULL) {
    c =nombre_cles_arbre_r(a->fgauche);
  }
  if (a->fdroite!=NULL) {
    b =nombre_cles_arbre_r(a->fdroite);
  }
  return 1+c+b ;*/
  
  int nbCle = 0;
  if(a != NULL) {
    if(feuille(a))  //si a est une feuille, 
      nbCle = 1;    //il n'y a qu'une clé.
    else {
      int nbCleG = nombre_cles_arbre_r(a->fgauche);
      int nbCleD = nombre_cles_arbre_r(a->fdroite);
      nbCle = nbCleG + nbCleD + 1;
    }
  }
  return nbCle;
  
}

int nombre_cles_arbre_nr (Arbre_t a) {
  /*  retourne le nombre de clé en incrémentant
   *  un compteur en parcourant l'arbre.
   */
  pfile_t f = creer_file();
  enfiler(f, a);
  int n = 1;
  while (!file_vide(f)) {
    Arbre_t sorti = defiler(f);
    if (sorti->fgauche != NULL) {
      n++;
      enfiler(f, sorti->fgauche);
    }
    if (sorti->fdroite != NULL) {
      n++;
      enfiler(f, sorti->fdroite);
    }
  }
  return n; 
}

int trouver_cle_min (Arbre_t a) {
  /* retourne la clé du plus petit neoud de l'arbre  */
  /* SEE
  ppile_t p=creer_pile();
  int pasfeuille=1;
  while (pasfeuille) {
  Arbre_t sorti =depiler(p);
    if (feuille(sorti)) {
      pasfeuille=0;
      return sorti->cle;
    }
  empiler(p,sorti->fgauche); 
  }
  return -1;*/

  if(a != NULL) {
    if(!feuille(a))
      return trouver_cle_min(a->fgauche);
    return a->cle;
  }
  return -1;
}

Arbre_t trouver_min (Arbre_t a) {
  /* retourne le plus petit neoud de l'arbre  */
  if(a->fgauche==NULL)
    return a;
  else
    return trouver_min(a->fgauche);
}
 

void imprimer_liste_cle_triee_r (Arbre_t a) {/* SEE
  if (feuille(a)) {
    printf("%d  ",a->cle);
  }else{
    if (a->fgauche!=NULL) {
      imprimer_liste_cle_triee_r(a->fgauche);
    }
    printf("%d  ",a->cle);
    if (a->fdroite!=NULL) {
      imprimer_liste_cle_triee_r(a->fdroite);
    }
  }*/
  if(a != NULL) {
    //if(a->fgauche != NULL)  pas besoin, cela est verifie par a!=NULL
    imprimer_liste_cle_triee_r(a->fgauche);
    printf("%d\n", a->cle);
    //if(a->fdroite != NULL)  idem
    imprimer_liste_cle_triee_r(a->fdroite);
  }
}

void imprimer_liste_cle_triee_nr (Arbre_t a) {
  ppile_t p = creer_pile();
  Arbre_t temporaire = a;
  while(temporaire != NULL) {
    empiler(p, temporaire);
    temporaire = temporaire->fgauche;
  }
  while(!pile_vide(p)) {
    Arbre_t sortie = depiler(p);
    printf("%d  ", sortie->cle);
    if(sortie->fdroite != NULL) {
      temporaire = sortie->fdroite;
      while(temporaire != NULL) {
      empiler(p, temporaire);
      temporaire = temporaire->fgauche;
      }
    }
  }
}
//idée de pile utiliser plusieurs facteur comment?? un compteur ??


int arbre_plein (Arbre_t a) {
  int hauteur = hauteur_arbre_r(a);
  int noeud = nombre_cles_arbre_r(a);
  int puissance = 1;

  for (int i = 1; i <= hauteur+1; i++)
    puissance *= 2;
  if((puissance-1) == noeud)  //h=2^(h+1)-1
    return 1;
  else
    return 0 ;
}


int arbre_parfait (Arbre_t a) {
  int hauteur=hauteur_arbre_r(a);
  int noeud=nombre_cles_arbre_r(a);
  
  int borneInf = 1;     //2^h
  int borneSup = 0;     //2^(h+1)-1

  for (int i=1; i<= hauteur; i++)
    borneInf *= 2;
  borneSup = 2*borneInf-1;
  if((borneInf <= noeud) && (noeud <= borneSup)) {  //2^h <= noeud <= 2^(h+1)-1
    /* maintenant, on vérifie que l'arbre est tassé à gauche  */
    pfile_t f = creer_file();
    int n = 0;
    int niveau = 1;
    int feuilleniv = 0;
  enfiler(f, a);
  while (!file_vide(f)) {
    Arbre_t sorti = defiler(f);
    niveau--;
    if (feuille(sorti))
      feuilleniv = 1;
    else{
      if(feuilleniv == 1)
        return 0;
    }
    if (sorti->fgauche != NULL) {
      enfiler(f, sorti->fgauche);
      n++;
    }
    if (sorti->fdroite != NULL) {
      enfiler(f, sorti->fdroite);
      n++;
    }
    if((niveau == 0) && (!file_vide(f))) {
      niveau = n;
      feuilleniv = 0;
      n = 0;
    }
  }
  return 1;
  }
  return 0;
}

pfile_t liste_cle_triee_r_file (Arbre_t a,pfile_t f) {
  if (feuille(a)) {
    enfiler(f,a);
  }else{
    if (a->fgauche!=NULL)
      f=liste_cle_triee_r_file(a->fgauche,f);
    enfiler(f,a);
    if (a->fdroite!=NULL)
      f=liste_cle_triee_r_file(a->fdroite,f);
  }
  return f;
}


Arbre_t rechercher_cle_sup_arbre (Arbre_t a, int valeur) {
    pfile_t f = creer_file();
    f = liste_cle_triee_r_file(a,f);
    Arbre_t temporaire = defiler(f);
    while(!file_vide(f) && temporaire->cle <= valeur)
      temporaire = defiler(f);
    if(temporaire->cle >= valeur)
      return temporaire;
  return NULL ;
}

Arbre_t rechercher_cle_inf_arbre (Arbre_t a, int valeur) {   
    pfile_t f = creer_file();
    f = liste_cle_triee_r_file(a,f);
    Arbre_t temporaire = defiler(f);
    Arbre_t min = temporaire;
    while(!file_vide(f) && temporaire->cle <= valeur) {
      temporaire = defiler(f);
      if (temporaire->cle < valeur)
        min = temporaire;
    }
    if(min->cle < valeur)
      return min;
  return NULL ;
  
}


Arbre_t detruire_cle_arbre (Arbre_t a, int cle) {
  if (a==NULL) {
    return NULL;
  }
  if(cle<a->cle) {
    
       a->fgauche=detruire_cle_arbre(a->fgauche,cle);
      return a;
      }
  if(cle>a->cle) {
       a->fdroite=detruire_cle_arbre(a->fdroite,cle);
      return a;
    }
  if (cle==a->cle) {
    return detruire_racine(a);
  }
  return NULL;
}

Arbre_t detruire_racine (Arbre_t a) {
  if (a->fgauche == NULL && a->fdroite == NULL)
    return NULL;

  if(a->fgauche == NULL)
    return a->fdroite;

  if(a->fdroite == NULL)
    return a->fgauche;

  if (a->fgauche != NULL && a->fdroite != NULL) {
    Arbre_t m = trouver_min(a->fdroite);
    a->cle = m->cle;
    a->fdroite = detruire_min(a->fdroite);
    return a;
  }

  return NULL;
}

Arbre_t detruire_min(Arbre_t a) {
  if (a->fgauche!=NULL) {    
    a->fgauche=detruire_min(a->fgauche);
    return a;
  }else{
    a=a->fdroite;
    return a;
  }
}

Arbre_t intersection_deux_arbres (Arbre_t a1, Arbre_t a2) {
  Arbre_t intersection=NULL;
  pfile_t f1=creer_file();
  f1=liste_cle_triee_r_file(a1,f1);
  pfile_t f2=creer_file();
  f2=liste_cle_triee_r_file(a2,f2);
  Arbre_t temp1=defiler(f1);
  Arbre_t temp2=defiler(f2);

  while (!file_vide(f1)&&!file_vide(f2)) {
    if (temp1->cle==temp2->cle) {
      intersection=ajouter_cle(intersection,temp1->cle);
      temp1=defiler(f1);
      temp2=defiler(f2);
    }else{
      if (temp1->cle<temp2->cle) {
        temp1=defiler(f1);
      }else{
        temp2=defiler(f2);
      }
    }
  }
  if (!file_vide(f1)) {
    if (temp1->cle==temp2->cle) {
      intersection=ajouter_cle(intersection,temp1->cle);
    }
    while (!file_vide(f1)) {
      temp1=defiler(f1);
      if (temp1->cle==temp2->cle) {
      intersection=ajouter_cle(intersection,temp1->cle);
    }
    }
  }else{
      if (temp1->cle==temp2->cle) {
      intersection=ajouter_cle(intersection,temp1->cle);
    }
    while (!file_vide(f2)) {
      temp2=defiler(f2);
      if (temp1->cle==temp2->cle) {
      intersection=ajouter_cle(intersection,temp1->cle);
    }
    }

  }
  return intersection ;
  
}

Arbre_t union_deux_arbres (Arbre_t a1, Arbre_t a2) {
  Arbre_t union_a=NULL;
  pfile_t f1=creer_file();
  f1=liste_cle_triee_r_file(a1,f1);
  pfile_t f2=creer_file();
  f2=liste_cle_triee_r_file(a2,f2);
  Arbre_t temp1=defiler(f1);
  Arbre_t temp2=defiler(f2);

  while (!file_vide(f1)&&!file_vide(f2)) {
    if (temp1->cle==temp2->cle) {
      union_a=ajouter_cle(union_a,temp1->cle);
      temp1=defiler(f1);
      temp2=defiler(f2);
    }else{
      if (temp1->cle<temp2->cle) {
        union_a=ajouter_cle(union_a,temp1->cle); 
        temp1=defiler(f1);
      }else{
        union_a=ajouter_cle(union_a,temp2->cle); 
        temp2=defiler(f2);
      }
    }
  }
  union_a=ajouter_cle(union_a,temp1->cle);
  union_a=ajouter_cle(union_a,temp2->cle);
  while (!file_vide(f1)) {
      temp1=defiler(f1);
      union_a=ajouter_cle(union_a,temp1->cle);
  }

  while (!file_vide(f2)) {
    temp2=defiler(f2);
    union_a=ajouter_cle(union_a,temp2->cle); 

  }
  return union_a ;
}

