#include <stdlib.h>

#include "../lib/abr.h"
#include "../lib/file.h"

pfile_t creer_file () {
  pfile_t f= malloc(sizeof(file_t));
  f->tete=0;
  f->queue=0;
  return f ;
}

int detruire_file (pfile_t f) {
  free(f);
  return 0 ;
}

int file_vide (pfile_t f) {
  if (f->tete==f->queue) {
    return 1;
  }
  return 0;
}

int file_pleine (pfile_t f) {
  if(((f->tete)%(MAX_FILE_SIZE+1))==(f->queue+1)%(MAX_FILE_SIZE+1)) {
    return 1;
  }
  return 0 ;
}

pnoeud_t defiler (pfile_t f) {
  if (!file_vide(f)) {
    pnoeud_t noeud= f->Tab[f->tete];
    f->tete+=1;
    f->tete=f->tete%(MAX_FILE_SIZE+1);
    return noeud;
  }
  return NULL ;
}

int enfiler (pfile_t f, pnoeud_t p) {
  if (!file_pleine(f)) {
    f->Tab[f->queue]=p;
    f->queue+=1;
    f->queue=f->queue%(MAX_FILE_SIZE+1);
    return 0;
  }
  return 1;
}
