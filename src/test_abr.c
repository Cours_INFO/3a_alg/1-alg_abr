#include <stdio.h>
#include <stdlib.h>

#include "../lib/abr.h"
#include "../lib/avl.h"

int main (int argc, char**argv) {
  Arbre_t a ;
  Arbre_t b;
  if (argc != 3)
    {
      fprintf (stderr, "il manque le parametre nom de fichier\n") ;
      exit (-1) ;
    }

  a = lire_arbre (argv[1]) ;
  b = lire_arbre (argv[2]) ;
  
  afficher_arbre (a,0) ;

  /*
     appeler les fonctions que vous
     avez implementees
  */
  printf("\n%d  =%d\n",hauteur_arbre_r(a),hauteur_arbre_nr(a));
  parcourir_arbre_largeur(a);
  printf("\n");
  afficher_nombre_noeuds_par_niveau(a);
  printf("\n");
  printf("\n%d  =%d\n",nombre_cles_arbre_r(a),nombre_cles_arbre_nr(a));
  printf("\n");
  imprimer_liste_cle_triee_r(a);
  printf("\n");
  imprimer_liste_cle_triee_nr(a);
  printf("\n");
  printf("%d",arbre_plein(a));
  printf("%d",arbre_parfait(a));
  afficher_arbre(rechercher_cle_inf_arbre(a,5),0);
  afficher_arbre(rechercher_cle_sup_arbre(a,5),0);
  Arbre_t c= intersection_deux_arbres(a,b);
  afficher_arbre(c,0);
  imprimer_liste_cle_triee_nr(a);
  printf("\n");    
  imprimer_liste_cle_triee_nr(b);
  printf("\n");
  imprimer_liste_cle_triee_nr(c);
  afficher_arbre(a,0);    
  printf("\n");   
  detruire_cle_arbre(a,2);
  printf("\n");   
  afficher_arbre(a,0);
  imprimer_liste_cle_triee_nr(a);
  AVL_t arbre=NULL;
  printf("\n");   
  printf("\n");   
  printf("\n");   
  int tab[7]={4,3,1,6,7,5,2};
  for(int i=0;i<7;i++) {
    arbre=ajout_cle(arbre,tab[i]);
    printf("\n================================================\n");
    afficher_arbre_avl(arbre,0);
  }
  arbre=destruction_cle(arbre,1);
  printf("\n================================================\n");
  afficher_arbre_avl(arbre,0);
}
