#include <stdio.h>
#include <stdlib.h>

#include "../lib/abr.h"
#include "../lib/pile.h"

int main() {
    ppile_t p=creer_pile();
    pnoeud_t noeud;
    printf("********EMPILER********\n");
    int plein = 0;
    int vide = 0;
    int i = 0;
    while(plein==0) {
        noeud=malloc(sizeof(pnoeud_t));
        noeud->cle=i;
        empiler(p,noeud);
        printf("empiler: %d\n",noeud->cle);
    
        if (pile_pleine(p)) {
            plein=1;
            printf("la pile est pleine: OK\n");
        }
        i++;
    }
    printf("********DEPILER********\n");
    while(vide==0) {
         noeud=depiler(p);
         printf("depiler: %d\n",noeud->cle); 
         free(noeud);   
        if(pile_vide(p)) {
            vide=1;
          printf("la pile est vide: OK\n");
        }
    }
    detruire_pile(p);
    return 0;
}