#include <stdio.h>
#include <stdlib.h>

#include "../lib/abr.h"
#include "../lib/pile.h"
#include "../lib/file.h"

int main() {
    pfile_t p=creer_file();
    pnoeud_t noeud;
    printf("********ENFILER********\n");
    int plein = 0;
    int i = 0;
    printf("%d  %d",p->queue,p->tete);
    while(plein==0) {
        noeud=malloc(sizeof(pnoeud_t));
        noeud->cle=i;
        enfiler(p,noeud);
        printf("enfiler: %d\n",noeud->cle);
        printf("%d  %d",p->queue,p->tete);
        if (file_pleine(p)) {
            plein=1;
            printf("la file est pleine: OK\n");
        }
        i++;
    }
    printf("********DEFILER********\n");
    for(i=0;i<10;i++) {
        noeud=defiler(p);
        printf("defiler: %d\n",noeud->cle); 
        free(noeud);           
    }
    plein=0;
    while(plein==0) {
        noeud=malloc(sizeof(pnoeud_t));
        noeud->cle=i;
        enfiler(p,noeud);
        printf("enfiler: %d\n",noeud->cle);
        printf("%d  %d",p->queue,p->tete);
        if (file_pleine(p)) {
            plein=1;
            printf("la file est pleine: OK\n");
        }
        i++;
    }
    int vide =0;
    while(vide==0) {
        noeud=defiler(p);
        printf("defiler: %d\n",noeud->cle); 
        free(noeud);   
        if(file_vide(p)) {
            vide=1;
          printf("la file est vide: OK\n");
        }
    }
    detruire_file(p);
    return 0;
}