#include <stdio.h>
#include <stdlib.h>

#include "../lib/avl.h"

#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
#define abs(a) ((a)<0?(-a):(a))

//=======================================
//avl
void afficher_arbre_avl (AVL_t a, int niveau) {
  /*
    affichage de l'arbre a
    on l'affiche en le penchant sur sa gauche
    la partie droite (haute) se retrouve en l'air
  */
  
  int i ;
  
  if (a != NULL)
      {
	afficher_arbre_avl (a->fdroite,niveau+1) ;
	
	for (i = 0; i < niveau; i++)
	  printf ("\t") ;
	printf (" %d / %d /(%d)\n\n", a->cle,a->bal, niveau) ;

	afficher_arbre_avl (a->fgauche, niveau+1) ;
      }
  return ;
}

AVL_t rotation_gauche(AVL_t a) {
    int balX=a->bal;
    int balY=a->fdroite->bal;
    AVL_t sorti=a->fdroite;
    a->fdroite=sorti->fgauche;
    sorti->fgauche=a;
    sorti->bal=balY-1+min(balX,0);
    sorti->fgauche->bal=balX-1-max(balY,0);
    return sorti;
}

AVL_t rotation_droite(AVL_t a) {
    int balY=a->bal;
    int balX=a->fgauche->bal;
    AVL_t sorti=a->fgauche;
    a->fgauche=sorti->fdroite;
    sorti->fdroite=a;
    sorti->bal=balX+1+max(balY,0);
    sorti->fdroite->bal=balY+1-min(balX,0);
    return sorti;
}

AVL_t double_rotation_gauche(AVL_t a) {
    a->fdroite=rotation_droite(a->fdroite);
    return rotation_gauche(a);
}

AVL_t double_rotation_droite(AVL_t a) {
    

    a->fgauche=rotation_gauche(a->fgauche);
    return rotation_droite(a);    
}

AVL_t equilibrage(AVL_t a) {
    if (a->bal == 2) {
      if (a->fdroite->bal >= 0) {
        return rotation_gauche(a);
      }else {
        a= double_rotation_gauche(a);
        a->bal=0;
        return a;
      }
    }else {
      if (a->bal == -2) {
        if (a->fgauche->bal <= 0) {
            return rotation_droite(a);
        
        }else { 
            a= double_rotation_droite(a);
            a->bal=0;
            return a;
        }
      }
      return a;
    }
}

AVL_t ajout_cle(AVL_t a,int cle) {
  if(a==NULL) {
    AVL_t n=(AVL_t) malloc (sizeof(noeud_avl_t)) ;
    n->cle = cle;
    n->bal=0;
    n->fgauche = NULL ;
    n->fdroite = NULL ;
    return n;
  }else{

  if(a->cle>cle) {
    if (a->fgauche!=NULL) {
      int bal=a->fgauche->bal;
      AVL_t new = ajout_cle(a->fgauche,cle);
      if(a->fgauche->bal!=bal) {
        a->bal=a->bal-abs(a->fgauche->bal);
      }
      a->fgauche=new;
      a = equilibrage(a);
      return a;
    }
    AVL_t n=(AVL_t) malloc (sizeof(noeud_avl_t)) ;
      n->cle = cle;
      n->bal=0;
      n->fgauche = NULL ;
      n->fdroite = NULL ;
      a->fgauche=n;
      a->bal-=1;
      return a;
  }
  if(a->cle<cle) {
    if (a->fdroite!=NULL) {
      int bal=a->fdroite->bal;
      AVL_t new = ajout_cle(a->fdroite,cle);

      if(a->fdroite->bal!=bal) {
        a->bal=a->bal+abs(a->fdroite->bal);
      }
      a->fdroite=new;
      a = equilibrage(a);
      return a;
    }
      AVL_t n=(AVL_t) malloc (sizeof(noeud_avl_t)) ;
      n->cle = cle;
      n->bal=0;
      n->fgauche = NULL ;
      n->fdroite = NULL ;
      a->fdroite=n;
      a->bal+=1;
      return a;
  }
  return a;
  }
}
// marche pas 
AVL_t destruction_cle (AVL_t a, int cle) {
  if (a==NULL) {
    return NULL;
  }
  if(cle<a->cle) {
    
       a->fgauche=destruction_cle(a->fgauche,cle);
      if (a->fgauche->bal==0) {
        a->bal=a->bal-1;
      }
      //a=equilibrage(a);
      return a;
      }
  if(cle>a->cle) {
       a->fdroite=destruction_cle(a->fdroite,cle);
       if (a->fdroite->bal==0) {
        a->bal=a->bal-1;
      }
      //a=equilibrage(a);
      return a;
    }
  if (cle==a->cle) {
    //return detruire_racine_avl(a);
    return a;
  }
  return NULL;
}

AVL_t detruire_racine_avl (AVL_t a) {
  if (a->fgauche==NULL&&a->fdroite==NULL) {

    return NULL;
  }
  if(a->fgauche==NULL) {

    return a->fdroite;
  }
  if(a->fdroite==NULL) {

    return a->fgauche;
  }
  if (a->fgauche!=NULL&&a->fdroite!=NULL) {
      
  AVL_t m=trouver_min_avl(a->fdroite);
  a->cle= m->cle;
  a->fdroite=detruire_min_avl(a->fdroite);
  return a;
  }
  return NULL;
}

AVL_t detruire_min_avl(AVL_t a) {
  if (a->fgauche!=NULL) {    
    a->fgauche=detruire_min_avl(a->fgauche);
    return a;
  }else{
    a=a->fdroite;
    if(a->fgauche->bal==0) {
        a->bal=a->bal+1;
      }
      //a=equilibrage(a);
    return a;
  }
}


AVL_t trouver_min_avl (AVL_t a) {
  if(a->fgauche==NULL) {
    return a;
  }else{
    return trouver_min_avl(a->fgauche);
  }
}
