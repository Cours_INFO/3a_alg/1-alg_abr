# TP1 - ABR : Arbre Binaire de Recherche

Travail Pratique dans le cadre du cours d'Algorithmique Avancée à **POLYTECH GRENOBLE**.

***

## **TABLE DES MATIÈRES**

* **[Sujet](./TP1ALG.pdf)**

* **[Travail](#travail)**

***

## **TRAVAIL**

### **1. Préparation du TP**

Prendre l'archive [ABR.tar.gz](./archive/ABR.tar.gz)

### **2. Piles**

| Fichiers |
|:--------:|
| [pile.c](./src/pile.c) |
| [pile.h](./lib/pile.h) |
| [test_pile.c](./src/test_pile.c) |

### **3. Files**

| Fichiers |
|:--------:|
| [file.c](./src/file.c) |
| [file.h](./lib/file.h) |
| [test_file.c](./src/test_file.c) |

### **4. Arbres ABR**

| Fichiers |
|:--------:|
| [abr.c](./src/abr.c) |
| [abr.h](./lib/abr.h) |
| [test_abr.c](./src/test_abr.c) |

### **5. Arbres AVL**

| Fichiers |
|:--------:|
| [avl.c](./src/avl.c) |
| [avl.h](./lib/avl.h) |
