LIB = lib/
SRC = src/
.PHONY: clean

all: test_abr test_file test_pile

abr.o: $(addprefix $(SRC), abr.c) $(addprefix $(LIB), abr.h) $(addprefix $(LIB), pile.h) $(addprefix $(LIB), file.h)
	gcc -Wall -c $<

avl.o: $(addprefix $(SRC), avl.c) $(addprefix $(LIB), avl.h)
	gcc -Wall -c $<

pile.o: $(addprefix $(SRC), pile.c) $(addprefix $(LIB), pile.h)
	gcc -Wall -c $<

file.o: $(addprefix $(SRC), file.c) $(addprefix $(LIB), file.h)
	gcc -Wall -c $<

test_abr.o: $(addprefix $(SRC), test_abr.c) $(addprefix $(LIB), abr.h) $(addprefix $(LIB), avl.h)
	gcc -Wall -c $<

test_abr: test_abr.o pile.o file.o abr.o avl.o
	gcc -o $@ $^

# 	TESTS 	#
test_pile.o: $(addprefix $(SRC), test_pile.c) $(addprefix $(LIB), pile.h) $(addprefix $(LIB), abr.h)
	gcc -Wall -c $<

test_pile: test_pile.o pile.o file.o abr.o
	gcc -o $@ $^

test_file.o: $(addprefix $(SRC), test_file.c) $(addprefix $(LIB), file.h) $(addprefix $(LIB), abr.h)
	gcc -Wall -c $<

test_file: test_file.o pile.o file.o abr.o
	gcc -o $@ $^

mrproper: clean
	rm -f test_abr test_file test_pile

clean:
	rm -f *.o *~