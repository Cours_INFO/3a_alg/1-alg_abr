
/* structure noeud presentee en cours */
typedef struct m {
  int cle;
  int bal;
  struct m *fgauche, *fdroite;
} noeud_avl_t, *pnoeud_avl_t ;

typedef pnoeud_avl_t AVL_t;

void afficher_arbre_avl (AVL_t a, int niveau);

AVL_t rotation_gauche(AVL_t a);

AVL_t rotation_droite(AVL_t a);

AVL_t double_rotation_gauche(AVL_t a);

AVL_t double_rotation_droite(AVL_t a);

AVL_t equilibrage(AVL_t a);

AVL_t ajout_cle(AVL_t a,int cle);

AVL_t destruction_cle(AVL_t a,int cle);


AVL_t detruire_racine_avl (AVL_t a);

AVL_t detruire_min_avl(AVL_t a);

AVL_t trouver_min_avl (AVL_t a);


